-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 26, 2018 at 03:20 AM
-- Server version: 5.6.39-83.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gtsaw1_hatgwez`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `desc`) VALUES
(1, 'Gawzni SIte about us', '<p>dhcbdbc csadjcjkcd scdjckacds dhkfkds fkajfbdf sfsdjkggdf cdjadjcdjc djcdjcndac dcdjcaalkcdjc dkcjdncndciecd cadkcjcjdcnjac ajkcdcjkdscj d caljdkcndajc dj cadjc djc jdaadjc dlj cldjc djc d jc dljcjdc d ca cc dja d djdjj d cdjcjdlsdld cdjcjs</p>');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` int(11) DEFAULT NULL,
  `online_status` enum('online','offline') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `email`, `password`, `permission`, `online_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'AhmedReda', 'reda@example.com', '$2y$10$VeMpeLMYeudpKekWYZ9v8ugrH1GaZyE0vgJRIg1zarzkwyWboI78u', 1, 'online', 'D5lrjxLQXUb4XfewgY58L7B85fkrBBFhF20iYiKQUpXXyQIQl4y6AnNkDIwv', '2018-06-09 22:00:00', '2018-06-09 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_to_client_messages`
--

CREATE TABLE `admin_to_client_messages` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `is_read` enum('seen','unseen') NOT NULL DEFAULT 'unseen',
  `body` longtext NOT NULL,
  `is_admin` enum('yes','no') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_to_client_messages`
--

INSERT INTO `admin_to_client_messages` (`id`, `sender_id`, `receiver_id`, `is_read`, `body`, `is_admin`, `created_at`, `updated_at`) VALUES
(15, 105, 1, 'unseen', 'اتمنى، مساعدتي لأيجاد شخص بالمواصفات التي ارغب', 'no', '2018-07-24 01:32:23', '2018-07-24 01:32:23'),
(16, 105, 1, 'unseen', 'ارجو الرد السريع', 'no', '2018-07-24 02:10:26', '2018-07-24 02:10:26');

-- --------------------------------------------------------

--
-- Table structure for table `blacklists`
--

CREATE TABLE `blacklists` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `friend_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'القاهرة', NULL, '2018-07-17 13:29:39'),
(27, 1, 'الشرقية', '2018-07-02 19:16:54', '2018-07-17 14:05:54'),
(28, 1, 'الإسكندرية', '2018-07-07 03:36:54', '2018-07-17 14:07:04'),
(29, 1, 'الإسماعيلية', '2018-07-17 14:08:44', '2018-07-17 14:08:44'),
(30, 1, 'الإسماعيلية', '2018-07-17 14:08:46', '2018-07-17 14:08:46'),
(31, 1, 'أسوان', '2018-07-17 14:08:55', '2018-07-17 14:08:55'),
(32, 1, 'أسيوط', '2018-07-17 14:09:01', '2018-07-17 14:09:01'),
(33, 1, 'الأقصر', '2018-07-17 14:09:08', '2018-07-17 14:09:08'),
(34, 1, 'البحر الأحمر', '2018-07-17 14:09:15', '2018-07-17 14:09:15'),
(35, 1, 'البحيرة', '2018-07-17 14:09:21', '2018-07-17 14:09:21'),
(36, 1, 'بني سويف', '2018-07-17 14:09:27', '2018-07-17 14:09:27'),
(37, 1, 'بورسعيد', '2018-07-17 14:09:34', '2018-07-17 14:09:34'),
(38, 1, 'جنوب سيناء', '2018-07-17 14:09:41', '2018-07-17 14:09:41'),
(39, 1, 'الجيزة', '2018-07-17 14:09:47', '2018-07-17 14:09:47'),
(40, 1, 'الدقهلية', '2018-07-17 14:09:53', '2018-07-17 14:09:53'),
(41, 1, 'دمياط', '2018-07-17 14:10:00', '2018-07-17 14:10:00'),
(42, 1, 'سوهاج', '2018-07-17 14:10:07', '2018-07-17 14:10:07'),
(43, 1, 'السويس', '2018-07-17 14:10:14', '2018-07-17 14:10:14'),
(44, 1, 'الشرقية', '2018-07-17 14:10:24', '2018-07-17 14:10:24'),
(45, 1, 'شمال سيناء', '2018-07-17 14:10:35', '2018-07-17 14:10:35'),
(46, 1, 'الغربية', '2018-07-17 14:10:41', '2018-07-17 14:10:41'),
(47, 1, 'الفيوم', '2018-07-17 14:10:49', '2018-07-17 14:10:49'),
(48, 1, 'القاهرة', '2018-07-17 14:10:57', '2018-07-17 14:10:57'),
(49, 1, 'القليوبية', '2018-07-17 14:11:02', '2018-07-17 14:11:02'),
(50, 1, 'قنا', '2018-07-17 14:11:10', '2018-07-17 14:11:10'),
(51, 1, 'كفر الشيخ', '2018-07-17 14:11:15', '2018-07-17 14:11:15'),
(52, 1, 'مطروح', '2018-07-17 14:11:23', '2018-07-17 14:11:23'),
(53, 1, 'المنوفية', '2018-07-17 14:11:27', '2018-07-17 14:11:27'),
(54, 1, 'المنيا', '2018-07-17 14:11:34', '2018-07-17 14:11:34'),
(55, 1, 'الوادي الجديد', '2018-07-17 14:11:42', '2018-07-17 14:11:42'),
(56, 12, 'ورزازات', '2018-07-17 14:12:37', '2018-07-17 14:12:37'),
(57, 12, 'الرباط', '2018-07-17 14:12:49', '2018-07-17 14:12:49'),
(58, 12, 'طنجة', '2018-07-17 14:13:32', '2018-07-17 14:13:32'),
(59, 12, 'الدار البيضاء', '2018-07-17 14:13:40', '2018-07-17 14:13:40'),
(60, 12, 'مراكش', '2018-07-17 14:14:17', '2018-07-17 14:14:17'),
(61, 12, 'أكادير', '2018-07-17 14:14:23', '2018-07-17 14:14:23'),
(62, 12, 'فاس', '2018-07-17 14:14:30', '2018-07-17 14:14:30'),
(63, 52, 'الرياض', '2018-07-17 14:15:30', '2018-07-17 14:15:30'),
(64, 52, 'الدرعية', '2018-07-17 14:15:35', '2018-07-17 14:15:35'),
(65, 52, 'الخرج', '2018-07-17 14:15:42', '2018-07-17 14:15:42'),
(66, 52, 'الدوادمي', '2018-07-17 14:15:49', '2018-07-17 14:15:49'),
(67, 52, 'المجمعة', '2018-07-17 14:16:02', '2018-07-17 14:16:02'),
(68, 52, 'القويعية', '2018-07-17 14:16:08', '2018-07-17 14:16:08'),
(69, 52, 'مكة المكرمة', '2018-07-17 14:16:31', '2018-07-17 14:16:31'),
(70, 52, 'المدينة المنورة', '2018-07-17 14:16:46', '2018-07-17 14:16:46'),
(71, 52, 'بريدة', '2018-07-17 14:16:52', '2018-07-17 14:16:52'),
(72, 52, 'الدمام', '2018-07-17 14:17:00', '2018-07-17 14:17:00'),
(73, 52, 'أبها', '2018-07-17 14:17:07', '2018-07-17 14:17:07'),
(74, 52, 'تبوك', '2018-07-17 14:17:14', '2018-07-17 14:17:14'),
(75, 52, 'حائل', '2018-07-17 14:17:24', '2018-07-17 14:17:24'),
(76, 52, 'عرعر', '2018-07-17 14:17:31', '2018-07-17 14:17:31'),
(77, 52, 'نجران', '2018-07-17 14:17:46', '2018-07-17 14:17:46'),
(78, 52, 'الباحة', '2018-07-17 14:17:51', '2018-07-17 14:17:51'),
(79, 52, 'سكاكا', '2018-07-17 14:18:02', '2018-07-17 14:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_description` text COLLATE utf8mb4_unicode_ci,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `other_person_description` text COLLATE utf8mb4_unicode_ci,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marraige_status` enum('first','second','third','fourth') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'first',
  `social_status` enum('single','married','divorced','willow') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `children_number` tinyint(4) DEFAULT '0',
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_color` enum('white','black','dark_brown','brown','7enty_dark','7enty_white') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `physique` enum('thin','medium_thin','sporty','fat','huge') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` enum('medium_school','high_school','university','PHD','self_study') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `financial_status` enum('poor','lower_than_medium','medium','more_than_medium','good','mastora','rich') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `health_details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `career_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online` enum('online','offline') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved` enum('approved','not_approved') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_approved',
  `member_ship` enum('free','premium') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'free',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `fname`, `lname`, `email`, `password`, `username`, `phone`, `gender`, `main_description`, `city_id`, `other_person_description`, `dob`, `marraige_status`, `social_status`, `children_number`, `weight`, `height`, `skin_color`, `physique`, `education`, `financial_status`, `health_details`, `career_field`, `job`, `salary`, `online`, `image`, `approved`, `member_ship`, `remember_token`, `created_at`, `updated_at`) VALUES
(23, 'Donald', 'Wiza', 'eweber@example.net', '$2y$10$QN6/nryPtAEjkUvMNbTc6.xiZziS5ic0rco2ydgld9AWr/sOK.7tW', 'piper.lueilwitz', '(570) 797-4867', 'male', '<p>ابحث عن عروسة مناسبة&nbsp;</p>', 1, 'Ut fugit voluptate et qui repudiandae. Non autem voluptatem iusto iure occaecati temporibus. Qui ipsa voluptatem quam possimus. Quis aliquam aut libero recusandae et quaerat accusamus.', '1997-11-18', 'first', 'single', 6, '87', '155', 'white', 'sporty', 'high_school', 'rich', 'مطلوب عروسة مناسبة', 'Painter and Illustrator', 'Lebsack LLC', '7', 'online', '1531815606.jpg', 'approved', 'premium', NULL, '2018-06-19 06:39:23', '2018-07-23 20:30:14'),
(24, 'Fredrick', 'Wilderman', 'laverne84@example.com', '$2y$10$BprhqTCH3ErFT3u7yjAA7.0eHABXOR5UCcF7c22xjm6G0YusEbUYu', 'rickie85', '290-686-5088', 'female', '<p>Ea recusandae velit aperiam reprehenderit numquam beatae. Non minima quibusdam totam quisquam enim. In ut quo rem tempora nostrum vitae. Numquam voluptate et adipisci necessitatibus sit.</p>', 1, 'Non et et adipisci totam accusamus deserunt dolore. Ducimus porro aut ex ea quam enim. Vero doloribus error architecto ut deserunt vel.', '1979-01-11', 'first', 'single', 2, '88', '185', 'white', 'sporty', 'high_school', 'rich', 'Et harum tempora veniam ut.', 'Graduate Teaching Assistant', 'Cormier-Beier', '6', 'online', '1531814167.jpg', 'approved', 'free', NULL, '2018-06-19 06:39:23', '2018-07-17 12:56:07'),
(25, 'Terry', 'Mills', 'madaline.jacobi@example.org', '$2y$10$T154CC7hzJW/dSoSjPojdOIyWp5Xoeq.AMxTYiweJqFusouXRyCp.', 'turner.feeney', '798.286.9207', 'female', '<p>Est distinctio nihil sunt odio quia illum quisquam. Fugit reiciendis illum deleniti optio. Porro et facere reprehenderit et.</p>', 1, 'Omnis modi voluptatum quo consectetur id eligendi mollitia. Ad est in sunt magnam mollitia porro.', '1999-05-02', 'first', 'single', 4, '93', '179', 'white', 'sporty', 'high_school', 'rich', 'Odio eos quam aut deserunt.', 'Tank Car', 'Wisoky, Crona and Bechtelar', '6', 'online', '1531814193.jpg', 'approved', 'free', NULL, '2018-06-19 06:39:23', '2018-07-17 12:56:33'),
(26, 'Yessenia', 'O\'Connell', 'nickolas39@example.net', '$2y$10$i9lT6cSIFC9TlsGBczBF1OARrADbbi8gDUeQUNpJd7IlpqvdRuXq.', 'omonahan', '(525) 801-9991 x95318', 'female', '<p>Doloribus dolor eos reiciendis ducimus quam totam adipisci. Porro animi sapiente eos accusamus magnam assumenda. Ipsum est quae magnam. Cumque quos et veritatis ratione.</p>', 1, 'Commodi est dolores quis id. Tenetur sint ipsum ratione et qui adipisci. Repellendus provident quis voluptatem velit odio vel. Sit dolorem nemo ea maxime soluta dolorum sit.', '1994-03-20', 'first', 'single', 2, '61', '183', 'white', 'sporty', 'high_school', 'rich', 'Ut libero aut ad velit.', 'Database Administrator', 'Schneider, Sipes and VonRueden', '2', 'online', '1531814171.jpg', 'approved', 'free', NULL, '2018-06-19 06:39:23', '2018-07-17 15:13:26'),
(27, 'Cecile', 'Nader', 'mertz.annamae@example.org', '$2y$10$84cXOLoiPuUso/ysiDm37.znxI7nQ9ayXnAFEbY/1CipESF4fEasC', 'jessica.bogan', '505-831-2105', 'female', '<p>Voluptas voluptatem porro qui sed voluptatibus doloribus. Dolor aut ut sit est corrupti tenetur doloremque. Doloremque autem omnis numquam vel.</p>', 1, 'Sint sed voluptatem qui aut modi qui. Deleniti quo dolorum aperiam rerum libero. Consequatur aliquam et voluptatem et ea.', '1973-03-02', 'first', 'single', 2, '106', '158', 'white', 'sporty', 'high_school', 'rich', 'Placeat possimus sit ea.', 'Assessor', 'Quitzon, Cummings and Dooley', '9', 'online', '1531814176.jpg', 'approved', 'free', NULL, '2018-06-19 06:39:24', '2018-07-18 16:00:59'),
(28, 'Ludwig', 'Mayer', 'nathanial75@example.com', '$2y$10$hL.JZtxeoml6AOe3bZS6nejSfenIlgjdPpm9ixzly.AN/txpn.Ls6', 'cfarrell', '278.417.0892', 'male', '<p>Architecto vero voluptas aliquam aut id iure. Odio ipsum facere fugiat quo corporis. Non alias hic amet natus iusto enim fuga. Magni quaerat ex optio voluptatem aperiam dicta velit.</p>', 1, 'Dolores atque provident blanditiis iure. Veniam itaque nulla qui rerum voluptatem omnis voluptates architecto. Soluta provident doloribus laudantium consequatur perspiciatis dolores quos.', '1983-07-20', 'first', 'single', 6, '68', '162', 'white', 'sporty', 'high_school', 'rich', 'Ipsum distinctio aut quo.', 'Percussion Instrument Repairer', 'Kiehn PLC', '1800', 'offline', '1531815580.jpg', 'approved', 'free', 'GgroEsaGrWfTnUUlIrzPHMldtFxZJImMWBUdrqQ8yaSLGd7TzApGzNjDQHvY', '2018-06-19 06:39:24', '2018-07-17 13:19:40'),
(30, 'fkgjkd', 'trretre', 'adrmin@gmail.com', '$2y$10$xjDPF5jRBMX2UlJo8xLjaev.9tlPEb7nXC/pycvTUEa9z2EivqVV6', 'admidf', '3243245', 'male', '<p>ghjghjhgj</p>', 1, 'ghjghjhgj', '02/11/1998', 'first', 'single', NULL, '76', '150', 'dark_brown', 'sporty', 'high_school', 'lower_than_medium', 'fdfdgdfg', 'sdfsdf', 'fgdfgfd', '10000', 'online', '1531815549.jpg', 'approved', 'free', NULL, '2018-06-27 16:52:24', '2018-07-17 13:19:09'),
(31, 'ashraf', 'Ahmed', 'ashraf.mohammed92@gmail.com', '$2y$10$WEW07FurzQYYY1CU9/9aXeO5uKtVPFch8B947krpF/C.VGHLKo22q', 'wwww', '01201481451', 'female', '<p>لايوجد</p>', 1, 'asdasd', '02/13/1998', 'first', 'single', NULL, '80', '175', 'brown', 'medium_thin', 'medium_school', 'more_than_medium', 'لايوجد', 'لايوجد', 'لايوجد', '2000', 'online', '1531815069.jpg', 'approved', 'free', NULL, '2018-06-27 18:52:17', '2018-07-17 13:11:09'),
(33, 'fkgjkd', 'trretre', 'hammada77777@yahoo.com', '$2y$10$5d84K6HYQshxqIAT/HMyiOfMsePDaMAsxkaGiZS88KtXpBPm8fpBy', 'admrt', '01128162458', 'female', NULL, 1, 'fdgdfgdf', '02/11/1998', 'first', 'single', NULL, '60', '150', 'dark_brown', 'medium_thin', 'university', 'poor', 'erteit', 'dfgd', 'fgdfgfd', '4000', 'online', '1531814273.jpg', 'approved', 'free', NULL, '2018-06-27 19:53:43', '2018-07-17 12:57:53'),
(34, 'Ahmed', 'Reda', 'reda19952018@gmail.com', '$2y$10$8aLoLpgzxPGkWTJSBwGPe.v1i6t9omi.EYE9gpMNEib9uEyFU33u2', 'redareda95', '01114388500', 'female', '<p>Am yood</p>', 1, 'She is good', '1995-10-19', 'first', 'single', 0, '75', '175', 'dark_brown', 'medium_thin', 'university', 'more_than_medium', 'Good', 'Gtsaw', 'مبرمج', '5000', 'online', '1531814330.jpeg', 'approved', 'free', 'eqid5rwzjOKG0qeZZSKraNTqqFwxUklIOFr0q9oVl37A4VsVpENuFXy3l5I6', '2018-06-28 01:32:03', '2018-07-17 12:58:50'),
(45, 'Ola', 'Saleh', 'ola@gtsaw.com', '$2y$10$dmmF6jNPUOTwYPN61A5Y0OBQZav0vlK5XmCHdbjM2TiNZfY6LFPcC', 'OlaSaleh94', '01004388500', 'female', '<p>Cool and fool</p>', 1, '`eat and seat', '02/09/1998', 'first', 'single', NULL, '55', '160', 'white', 'thin', 'PHD', 'medium', 'batny wag3any', 'Gtsaw', 'Manager', '10000', 'online', '1531814285.jpg', 'approved', 'free', NULL, '2018-07-02 17:33:33', '2018-07-17 12:58:05'),
(46, 'Ahmed', 'Eid', 'osama139421@gmail.com', '$2y$10$ElsgL1zVzDnoiQkfghdsY.In/8YqOC5ToMSzojQSrOesvVgGz7CnG', 'Ahmed', '01022365483', 'female', '<p>5156156</p>', 1, '3213\r\n31', '02/11/1994', 'first', 'single', NULL, '85', '180', 'white', 'sporty', 'university', 'medium', 'جيدة', 'كمبيوتر', 'مهندس', '2001', 'online', '1531814290.jpg', 'approved', 'free', NULL, '2018-07-02 17:35:28', '2018-07-17 12:58:10'),
(47, 'مني', 'محمد', 'mona@gmail.com', '$2y$10$S0xbTkm90IXK9zxNVyDfzuI.5mkQbts8U61ARsxppFoQem9TR8D8O', 'مني', '01230254156', 'female', '<p>جميله جدا رشيقه جدا عميقه جدا</p>', 1, 'تافهه جدا', '5/5/1980', 'first', 'divorced', 3, '55', '158', 'white', 'sporty', 'university', 'good', 'جيده', 'الصيدليه', 'دكتوره صيدليه', '20000', 'online', '1531814301.jpg', 'approved', 'free', NULL, '2018-07-02 17:38:33', '2018-07-17 12:58:21'),
(48, 'عبدالمنعم', 'محمود', 'mon3am@gmail.com', '$2y$10$AtvZUjZr/tcKg2wxCsagGOvOSfEPS4gBECx/cqGy5xlC.bIKGqqAC', 'منعم', '01478523695', 'female', '<p>مدير تسويق ودعايا والاعلان طويل القامه اشقر الشعر جذاب لدرجه العذاب قوي البينه وقوي الشتيمه</p>', 1, 'علاقه جاده من جده ... تجيد التسويق الالكتروني رشيقه ليس لديها اطفال بنت حسب ونسب', '22/2/1970', 'first', 'divorced', 8, '100', '120', 'black', 'medium_thin', 'PHD', 'rich', 'جيده جدا اكسر الباب برجل واحده', 'شركات كتيره', 'مدير التسويق الالكتروني', '21254', 'online', '1531814312.jpg', 'approved', 'free', NULL, '2018-07-02 17:47:24', '2018-07-17 12:58:32'),
(49, 'وليد', 'حسد', 'captwell@gmail.com', '$2y$10$nicU1Rr5HotfBzbwdv2/hedL0qIQMkpgWHsBELqtsevFLjlrE4AvC', 'well', '01000494200', 'female', '<p>لار ت راترلت</p>', 1, 'لبتبتلابتا', '2018-07-02', 'first', 'single', NULL, '35', '150', 'white', 'thin', 'medium_school', 'poor', 'fdgdgdf', 'sdfsdf', 'dsfdsf', '1100', 'online', '1531814447.jpg', 'approved', 'free', NULL, '2018-07-02 18:06:49', '2018-07-17 13:00:47'),
(50, 'Jarvis', 'Thiel', 'barton.marielle@example.org', '$2y$10$Jl4ke/7AkgOVn7UviJj5/e5tSnmdIYU9kccioiZlBG1vtaTWIFzBm', 'qfunk', '1-802-310-2753 x75277', 'female', '<p>Perspiciatis ea enim voluptatibus alias. Molestiae porro rerum debitis labore minima facilis. Sit ratione rerum eius adipisci sit eaque dicta eos.</p>', 1, 'Consequatur aspernatur praesentium doloremque iusto error ipsum eaque sint. Aut ab eaque consequatur quod aut. Dolores voluptate laboriosam ullam occaecati et.', '1973-07-09', 'first', 'single', 3, '79', '197', 'white', 'sporty', 'high_school', 'rich', 'Molestiae velit nobis ut.', 'Social Worker', 'Herzog Inc', '2', 'online', '1531814361.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:44', '2018-07-17 12:59:21'),
(51, 'Novella', 'Rosenbaum', 'eusebio.mertz@example.org', '$2y$10$xhntqiBWrKYT5r8yw6nRq.L7OrKvs3akLETAZcqP6NfF5HPqHP6EG', 'rosenbaum.willow', '646-950-2958', 'female', '<p>Et cupiditate voluptatibus dolor rerum facilis. Et accusantium non velit et dolorem aliquam. Aliquam perspiciatis voluptatem magni in ut voluptas nulla. Et voluptatem qui sit neque ab.</p>', 1, 'Maxime nihil sit in quis enim. Enim totam qui sit placeat nisi quaerat molestiae.', '1993-12-05', 'first', 'single', 1, '64', '181', 'white', 'sporty', 'high_school', 'rich', 'Qui nemo rem voluptatibus.', 'Forest and Conservation Worker', 'Cummerata-Walsh', '2', 'online', '1531814352.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:44', '2018-07-17 12:59:12'),
(52, 'Dayton', 'Berge', 'otto.kemmer@example.org', '$2y$10$LvRkORFGOs.9WjQS6w1rB.0OVIVgIUxBU1WtlPG1tCI1zAeWPlgoa', 'chaim.paucek', '1-294-925-1587 x432', 'female', '<p>Voluptas recusandae dolor magni quo maiores corrupti. Nihil id eum et molestiae dolorem. Omnis temporibus quia officiis est ipsa. Consequatur velit cum eos sit sed.</p>', 1, 'Velit dolor voluptas unde possimus qui voluptatem. Consectetur et nemo est quia et. Id dolorem voluptatem atque et consequatur amet minus.', '1985-04-08', 'first', 'single', 5, '70', '166', 'white', 'sporty', 'high_school', 'rich', 'Ad similique ipsam aut eius.', 'Electronic Engineering Technician', 'Muller Group', '8', 'online', '1531814355.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:44', '2018-07-17 12:59:15'),
(53, 'Theo', 'Harvey', 'friesen.dillon@example.org', '$2y$10$ZLgzRPAXhlwDxtYXdzN1YuFGPMECN37kOAQWazWUH7dyR.PJh.7BG', 'kaycee17', '(441) 976-6118 x108', 'male', '<p>Architecto aut ea maxime recusandae saepe molestiae aut aut. Ut magnam ut eum et illum ut sint. Dolorem est voluptas aspernatur.</p>', 1, 'Animi ipsum et quod odit corporis ratione. Dolorem omnis voluptas autem voluptatem nihil veniam est. Neque et id exercitationem qui corporis ut. Reprehenderit culpa commodi illo modi aut.', '1978-11-26', 'first', 'single', 5, '65', '172', 'white', 'sporty', 'high_school', 'rich', 'Quia accusantium et est.', 'Forestry Conservation Science Teacher', 'Reichert, Kihn and Dooley', '6', 'online', '1531817483.jpeg', 'approved', 'free', NULL, '2018-07-02 19:14:44', '2018-07-17 13:51:23'),
(54, 'Jerod', 'Bechtelar', 'lillie.fritsch@example.org', '$2y$10$8TO2gFNYlqM/3BXBNiym1.NvK3NLcrnbxZb78JJcvJmYjyEb5H/ji', 'tgrimes', '432-660-2127', 'male', '<p>Voluptatibus iste at accusamus velit. Dolores sapiente quia iure corrupti sit eum.</p>', 1, 'Et ut earum quod dignissimos nam voluptatem commodi perspiciatis. In aut odio molestiae tempora. Voluptatem atque ducimus nostrum veritatis provident. Dolores eos et veritatis ut.', '1992-08-28', 'first', 'single', 1, '75', '181', 'white', 'sporty', 'high_school', 'rich', 'Illum est amet eum veniam.', 'Directory Assistance Operator', 'Crist-Schmidt', '3', 'online', '1531815670.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:44', '2018-07-17 13:21:10'),
(55, 'Dante', 'Cruickshank', 'vnader@example.org', '$2y$10$yARNnmYprCki2C94QaGKauEILWX5phMFMDpNyZfiHe46kW.HfCpSu', 'arvid25', '910-603-7987', 'male', '<p>Ut nisi ut in fuga minus. Nobis aut enim ut voluptatem. Ea consequatur sint aliquid vitae cumque odit.</p>', 1, 'Voluptate quos totam odio perferendis tenetur omnis quas. Dicta vel et optio sed ducimus earum. Ut dignissimos incidunt ducimus sequi quia.', '1972-10-13', 'first', 'single', 1, '87', '150', 'white', 'sporty', 'high_school', 'rich', 'Aut sed voluptas dicta at id.', 'Ship Mates', 'Marvin-Aufderhar', '5', 'online', '1531815823.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:45', '2018-07-17 13:23:43'),
(56, 'Wendell', 'Langosh', 'greta20@example.net', '$2y$10$cAAsy13F.tjRrSWfbuu4eeLs6AHb1csSfC5azojA6FlTx7pxkSH1y', 'meda00', '1-428-674-5888 x04207', 'male', '<p>Voluptas nihil excepturi ad reprehenderit et. Quia voluptatem nisi consequatur dolor dolor sint. At et et quia.</p>', 1, 'Aut quas est aut consequuntur. Animi cumque vitae magnam ut autem sint sapiente sit. Iusto ipsum consequatur voluptatem perferendis.', '1970-03-07', 'first', 'single', 2, '66', '178', 'white', 'sporty', 'high_school', 'rich', 'Placeat nesciunt qui quia.', 'Management Analyst', 'Stoltenberg LLC', '3', 'online', '1531815702.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:45', '2018-07-17 13:21:42'),
(57, 'Willy', 'Sanford', 'yessenia73@example.org', '$2y$10$lwSPRKtBSjq/DA94cF9mVu82g1Do0JlJPUKp9.4wTN6ZVjcD32SbG', 'koss.isidro', '+17347556944', 'male', '<p>Quia voluptatum tempora odio et consequatur earum quam. Omnis architecto blanditiis velit ratione fugiat et. Occaecati veniam reiciendis vel aut.</p>', 1, 'Facilis nisi id accusantium quidem quis. Est ut praesentium veniam expedita. Ut in optio vel quia quas eos. Facilis sequi amet laboriosam qui veritatis provident.', '1988-11-22', 'first', 'single', 3, '87', '146', 'white', 'sporty', 'high_school', 'rich', 'Vero incidunt unde ea ut et.', 'Model Maker', 'Paucek Ltd', '2', 'online', '1531815725.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:45', '2018-07-30 19:56:34'),
(58, 'Myra', 'Herzog', 'bayer.amiya@example.net', '$2y$10$5utEG1MumMzwi.UsF4hJlu58r0I1TF1W05rBmncvvPUDyWtoD4mnC', 'oleta.cummerata', '+1-756-233-3863', 'male', '<p>Rem dolorem ipsum earum dignissimos. Voluptas fugit dignissimos perferendis mollitia voluptatibus quis. Repellat quibusdam praesentium numquam.</p>', 1, 'Rerum totam assumenda autem. Sit quod vero vel. Eligendi iusto itaque et nihil eaque corrupti vero.', '1989-05-05', 'first', 'single', 5, '62', '174', 'white', 'sporty', 'high_school', 'rich', 'Atque fuga aut totam aut et.', 'Composer', 'Wolf-Emard', '5', 'online', '1531815749.jpg', 'approved', 'free', NULL, '2018-07-02 19:14:45', '2018-07-17 13:22:29'),
(59, 'شششش', 'يييي', 'moniwebas@yahoo.com', '$2y$10$9eI5TTYQkB/E.7w0kHLtH.KV8OH5M4zWIp223gd1F1BJGmT0qskbK', 'abdo132007', '01023651134', 'male', '<p>بحب السفر&nbsp;</p>', 1, 'يسشيش شسي', '02/05/1998', 'first', 'married', 34, '50', '51', 'white', 'medium_thin', 'high_school', 'lower_than_medium', 'بثصقث صقذ', 'بسيب سي', 'يبسيب', '6666', 'online', '1531819920.jpg', 'approved', 'free', NULL, '2018-07-03 20:47:19', '2018-07-17 14:32:00'),
(60, 'Cary', 'Green', 'luettgen.alejandra@example.net', '$2y$10$Ih6cTtW76ZHdkZ.X4v.Y/uci249UpGNvf0HJ7DU7dYLPoSX7HKGAC', 'ucasper', '(553) 698-0213 x8055', 'female', 'Provident mollitia hic aut doloribus sed rerum repellendus. Libero nihil neque sit sit deleniti. Accusamus molestias voluptatem iusto et consequatur ut. Iste deleniti autem quia deserunt voluptatem.', 1, 'Quas sunt adipisci quis saepe harum quidem. Magnam odit assumenda natus assumenda. Adipisci ut est et quis sint odit. Neque impedit quasi exercitationem ullam quia.', '1971-01-24', '', 'single', 4, '105', '189', 'white', 'sporty', 'high_school', 'rich', 'Ut et enim non.', 'Press Machine Setter, Operator', 'Pfeffer-Bogisich', '9', 'offline', 'dddd8fc1d31f462c81df760e1e0e1acf.jpg', 'approved', 'free', 'vhfY1WI2DMOKdfUpcJyiJFW68YGBD835hxvnQo1mUivhP354XP1K2FbnbQay', '2018-07-07 03:22:27', '2018-07-15 13:59:22'),
(61, 'Ebba', 'Hudson', 'chanel26@example.org', '$2y$10$5XQhgdclSyerQr8YAYP7zObv2mZkzTXB/tqwRdXKMO9UemVbdXu9i', 'jacobi.clay', '1-238-287-5310 x271', 'male', '<p>Aspernatur officiis quisquam occaecati mollitia voluptas nemo dicta. Officiis fugiat ut explicabo. Esse impedit ipsa et nam. Voluptatem sed dolor eum.</p>', 1, 'Corporis inventore beatae assumenda voluptas. Natus cumque voluptatem in optio cum. Officiis impedit quia modi soluta minus. Iste fugit corporis non cupiditate.', '1984-06-13', 'first', 'single', 1, '63', '158', 'white', 'sporty', 'high_school', 'rich', 'Id nihil quisquam quibusdam.', 'Audio and Video Equipment Technician', 'Schimmel, Mante and Feeney', '3', 'online', '1531815636.jpg', 'approved', 'free', NULL, '2018-07-07 03:22:27', '2018-07-17 15:10:34'),
(62, 'Beaulah', 'Miller', 'rita66@example.org', '$2y$10$A1XHk8OjA0AIeud1YFYuGudta.rW/YACgpoU2swlxn9JcpcRcusXW', 'mervin46', '+1-734-410-1406', 'male', '<p>Non voluptas excepturi iste est. Aut ut eveniet tempore nam iure. Atque deleniti quos saepe qui qui sequi expedita.</p>', 1, 'Architecto ut odit aut rerum. Voluptatem itaque pariatur officia ea. Vel et doloribus rerum aspernatur dolore. Asperiores quia nisi sunt.', '1996-02-28', 'first', 'single', 5, '70', '179', 'white', 'sporty', 'high_school', 'rich', 'Omnis libero et consequatur.', 'Merchandise Displayer OR Window Trimmer', 'Hansen-Will', '1', 'online', '1531819830.jpg', 'approved', 'free', NULL, '2018-07-07 03:22:28', '2018-07-17 14:30:30'),
(92, 'مى', 'احمد', 'admin@gmail.com', '$2y$10$3/rQ.t.Tw8IwNroFfH6Wl.SZNyVSYPnbmW/.nKfhUAa0C2nMl2Syy', 'مى احمد', '01021121', 'female', 'احب الزوج الرومانسى', 1, 'يكون مثقف', '1990-07-17', 'first', 'single', 0, '50', '150', 'white', 'fat', 'university', 'more_than_medium', 'تمام', 'لايوجد', 'لايوجد', '250', 'online', 'a6a8d081ca93264036321038b7071d10.jpg', 'approved', 'free', 'qZ1fvjN1ClRwDJ09MBpqXcqA8deJ3kJpNbQgld1Tj5Ej7TGr70ukTpWBkfdM', '2018-07-17 13:26:19', '2018-08-06 20:06:28'),
(102, 'محمد', 'محمود', 'mohamed.hassan9377@gmail.com', '$2y$10$LswDOX3r1rPf.K8CW0D2KeUCOciLFWse6Ga9XPZTI2IUijh3TD0OG', 'محمد محمود', '01099840293', 'male', NULL, 1, NULL, '1977-03-09', 'first', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'online', '4cc8d2dd233ab11700bc3d85037db0c3.jpeg', 'approved', 'free', NULL, '2018-07-21 15:36:17', '2018-07-22 18:34:39'),
(103, 'احمد', 'مرسى', 'Hatgwez@gmail.com', '$2y$10$9JV6JqLqcchSggiuIgOTZ.kwKwXpl1CV.TiNuZHEOjwEFaz125vpm', 'احمد عتريس', '010235142252', 'male', NULL, 1, NULL, '1985-02-22', 'first', 'single', 0, NULL, NULL, 'white', 'thin', 'medium_school', 'poor', NULL, NULL, NULL, NULL, 'online', '68133eb840ec088b8588cf23557777e2.jpg', 'approved', 'free', NULL, '2018-07-23 17:21:01', '2018-07-25 20:59:12'),
(104, 'محمد', 'علي', 'mohamedalidesign80@gmail.com', '$2y$10$GA5joWPbMHHs.fhkGoX34OjifukMOmPL/Ipix/waGdxA/aUc0rxmu', 'محمد علي', '٠١٠٠٥١٨٠٣٤٢', 'male', NULL, 1, NULL, '1980-10-06', 'first', 'divorced', 0, '70', '170', 'white', 'medium_thin', 'medium_school', 'medium', 'Good', 'Decor', 'Decor', '5000', 'online', 'e14ed6c24d38f0f0c9e352237e1ee523.JPG', 'approved', 'free', '1PGXZdDmqqodVtHguGmLkYU34zKctei3Sp0Uiq9hJfGQQEG6j24unxsK2eZ1', '2018-07-23 21:39:45', '2018-07-23 22:18:10'),
(105, 'محمود', 'جاسم', 'medo19952009@hotmal.com', '$2y$10$RsriFMD7ufdaDtZPE3RrWOALBehzJCDbS5aG8CmupWiB1R1QRm36m', 'DeepTalker', '0553285512', 'male', 'انا مهندس تكيف عراقي الجنسية، كنت من انصار صدام حسين لهذا انا مكروه من الناس، اريد شريكة ذات خلفية سياسية', 75, 'تجيد الكلام في السياسة و الكلام العميق جدا', '1990-07-20', 'first', 'married', 2, '84', '180', 'dark_brown', 'medium_thin', 'university', 'more_than_medium', 'مدخن شره، لكن انوي الاقلاع', 'مهندس تكيف', 'شركة بن لادن', '20,000', 'online', '752e61c8133dc41972395c1f4ebf99b0.jpg', 'approved', 'free', 'VG20gjSHWi2bWPdHjndE56ez6exzWh7HKiiH7zJ4YWlgsYKbSWlYbZGPRL6a', '2018-07-24 01:21:49', '2018-07-24 02:10:03'),
(106, 'becky', 'sage', 'becky@gmail.com', '$2y$10$DfXGi0KINgLDcqdjeiHCiOIYsNXDoFdx.mZX.F/YY2D3vLQtFwpZC', 'Becko@gmail.com', '20347544', 'female', 'مغربية لكن احب المصريين', 59, 'مرح، و يتكلم كثيرا', '1960-07-23', 'first', 'married', 0, '52', '155', '7enty_dark', 'thin', 'PHD', 'good', 'اعاني من انيميا', 'مدير مبيعات', 'Pamperz', '60,000', 'offline', 'a5cdb113cd9653ef5d25c3106dad3159.jpg', 'approved', 'free', 'Nn02VW5CCK8VzIns3ZV2AnsLfpeKwXYQLdjjkB8fD9FQtMmbkxbeYyOjuIRY', '2018-07-24 01:52:00', '2018-07-24 02:50:20'),
(107, 'حسين', 'محمود', 'husinfarouk@yahoo.com', '$2y$10$wIQp3hE9enFFOaDU4g1JSuG6JANYI3Mqyt06OaD57lY9nvRAfTLSi', 'الحسين محمود', '01005134263', 'male', NULL, 39, NULL, '1971-03-16', 'first', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'online', 'e4002e2bfe49f5e9c5826929e8e5a47d.jpg', 'approved', 'free', NULL, '2018-07-25 06:44:46', '2018-07-25 20:57:48'),
(108, 'Tarek', 'Galal', 'niceperson20202000@yahoo.com', '$2y$10$rcsksse4BXsBJLhbCZMRQ.ZctnlnlRJ9zEZjvk3oScBAFk88Lijga', 'wega888', '01026619976', 'male', 'Funny', 1, 'Funny', '1984-01-10', 'first', 'married', 1, '92', '170', 'white', 'medium_thin', 'medium_school', 'medium', 'Normal', 'Business', 'المدير التنفيذي', '5000', 'online', '57819caf78dc27f4dcd0048a86a092e3.jpg', 'approved', 'free', NULL, '2018-07-25 17:51:28', '2018-07-25 20:57:53'),
(109, 'وليد', 'نور الدين', 'noureldeenw63@gmail.com', '$2y$10$7VH6mjCJaNWTUpqQwhjvBuptAddz6df/esKCwSpkngxer6AKvLN12', 'waleed nour eldeen', '01111892814', 'male', NULL, 1, NULL, '1963-08-03', 'first', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'online', '3189f4566d2733eb9ae47b0eacdf9d02.png', 'approved', 'free', NULL, '2018-07-28 17:31:24', '2018-07-31 16:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `client_password_resets`
--

CREATE TABLE `client_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_password_resets`
--

INSERT INTO `client_password_resets` (`email`, `token`, `created_at`) VALUES
('reda19952013@gmail.com', '$2y$10$aeGeqfPeOvvLYHEqb1T/Se0GsdMy8shWvZHfz7PBbgSGhC6OatURm', '2018-07-24 01:40:28');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone_number1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone1` int(11) NOT NULL,
  `phone2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `address`, `phone1`, `phone2`) VALUES
(1, 'sakr quraish sheraton', 1114388555, 1118855223);

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL,
  `client_1` int(11) NOT NULL,
  `client_2` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `client_1`, `client_2`, `created_at`, `updated_at`) VALUES
(19, 44, 81, '2018-07-16 19:51:52', NULL),
(20, 27, 24, '2018-07-18 16:10:21', NULL),
(21, 105, 46, '2018-07-24 01:32:51', NULL),
(22, 105, 103, '2018-07-24 01:36:27', NULL),
(23, 106, 105, '2018-07-24 01:56:03', NULL),
(24, 105, 102, '2018-07-24 02:06:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `currency`, `created_at`, `updated_at`) VALUES
(1, 'مصر', 'egp', NULL, '2018-07-17 13:29:28'),
(12, 'المغرب', 'SEK', '2018-06-12 10:08:37', '2018-07-17 13:52:10'),
(52, 'السعودية', 'ar', '2018-07-07 03:26:26', '2018-07-07 03:26:26'),
(53, 'لبنان', 'لبنان', '2018-07-17 14:00:14', '2018-07-17 14:00:14'),
(54, 'العراق', 'العراق', '2018-07-17 14:00:40', '2018-07-17 14:00:40'),
(55, 'سوريا', 'سوريا', '2018-07-17 14:00:48', '2018-07-17 14:00:48'),
(56, 'السعودية', 'السعودية', '2018-07-17 14:00:58', '2018-07-17 14:00:58'),
(57, 'اليمن', 'اليمن', '2018-07-17 14:01:04', '2018-07-17 14:01:04'),
(58, 'السودان', 'السودان', '2018-07-17 14:01:10', '2018-07-17 14:01:10'),
(59, 'الأردن', 'الأردن', '2018-07-17 14:01:18', '2018-07-17 14:01:18'),
(60, 'الإمارات', 'الإمارات', '2018-07-17 14:01:22', '2018-07-17 14:01:22'),
(61, 'ليبيا', 'ليبيا', '2018-07-17 14:01:28', '2018-07-17 14:01:28'),
(62, 'المغرب', 'المغرب', '2018-07-17 14:01:34', '2018-07-17 14:01:34'),
(63, 'فلسطين', 'فلسطين', '2018-07-17 14:01:40', '2018-07-17 14:01:40'),
(64, 'تونس', 'تونس', '2018-07-17 14:01:47', '2018-07-17 14:01:47'),
(65, 'عمان', 'عمان', '2018-07-17 14:01:54', '2018-07-17 14:01:54'),
(66, 'الكويت', 'الكويت', '2018-07-17 14:01:59', '2018-07-17 14:01:59'),
(67, 'الصومال', 'الصومال', '2018-07-17 14:02:11', '2018-07-17 14:02:11'),
(68, 'البحرين', 'البحرين', '2018-07-17 14:02:16', '2018-07-17 14:02:16'),
(69, 'الجزائر', 'الجزائر', '2018-07-17 14:02:22', '2018-07-17 14:02:22'),
(70, 'قطر', 'قطر', '2018-07-17 14:02:28', '2018-07-17 14:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`) VALUES
(1, 'why are we good?', '<p>odod dsmds sjdnsjd sjkdamkdf abdul-wahab</p>'),
(2, 'why are we bad?', 'hdhsd sdsdjs sudewudhe sndsndhns');

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `friend_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`id`, `client_id`, `friend_id`, `created_at`, `updated_at`) VALUES
(27, 49, 47, '2018-07-02 19:08:13', '2018-07-02 19:08:13'),
(28, 49, 49, '2018-07-02 19:10:07', '2018-07-02 19:10:07'),
(38, 34, 55, '2018-07-02 19:38:43', '2018-07-02 19:38:43'),
(45, 34, 59, '2018-07-03 23:38:39', '2018-07-03 23:38:39'),
(60, 92, 23, '2018-07-17 14:25:25', '2018-07-17 14:25:25'),
(61, 92, 24, '2018-07-17 14:25:26', '2018-07-17 14:25:26'),
(62, 92, 25, '2018-07-17 14:25:26', '2018-07-17 14:25:26'),
(63, 92, 26, '2018-07-17 14:25:28', '2018-07-17 14:25:28'),
(64, 92, 27, '2018-07-17 14:25:28', '2018-07-17 14:25:28'),
(65, 92, 28, '2018-07-17 14:25:30', '2018-07-17 14:25:30'),
(67, 92, 31, '2018-07-17 14:25:32', '2018-07-17 14:25:32'),
(68, 92, 33, '2018-07-17 14:25:33', '2018-07-17 14:25:33'),
(69, 92, 34, '2018-07-17 14:25:34', '2018-07-17 14:25:34'),
(71, 92, 46, '2018-07-17 14:25:35', '2018-07-17 14:25:35'),
(72, 92, 58, '2018-07-17 14:29:43', '2018-07-17 14:29:43'),
(77, 105, 51, '2018-07-24 01:31:43', '2018-07-24 01:31:43'),
(78, 105, 103, '2018-07-24 01:37:27', '2018-07-24 01:37:27'),
(79, 106, 105, '2018-07-24 01:55:58', '2018-07-24 01:55:58'),
(80, 105, 106, '2018-07-24 02:08:52', '2018-07-24 02:08:52');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `conversation_id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `read` enum('seen','unseen') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unseen',
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `conversation_id`, `sender_id`, `receiver_id`, `read`, `body`, `created_at`, `updated_at`) VALUES
(55, 21, 105, 46, 'unseen', 'كيف اسمك احمد و بصورة بنت يا شاذ يا قوم لوط', '2018-07-24 01:33:22', '2018-07-24 01:33:22'),
(56, 21, 105, 46, 'unseen', 'كلب ولا تسوى', '2018-07-24 01:35:33', '2018-07-24 01:35:33'),
(57, 22, 105, 103, 'unseen', 'احلى مسا عليك يا عم عتريس', '2018-07-24 01:36:40', '2018-07-24 01:36:40'),
(58, 22, 105, 103, 'unseen', 'ايه اللي جابك هنا يا بن عمي', '2018-07-24 01:36:58', '2018-07-24 01:36:58'),
(59, 23, 106, 105, 'unseen', 'اهلا شاب عراقي عميق', '2018-07-24 01:56:16', '2018-07-24 01:56:16'),
(60, 23, 106, 105, 'unseen', 'ممكن اشاركك في السياسة', '2018-07-24 01:56:27', '2018-07-24 01:56:27'),
(61, 23, 105, 106, 'unseen', 'طب ما تيجي نيجي كلنا الاول', '2018-07-24 01:58:28', '2018-07-24 01:58:28'),
(62, 24, 105, 102, 'unseen', 'اهلا استاذ محمد، هل لاسمك علاقة باحداث محمد محمود في مصر', '2018-07-24 02:07:29', '2018-07-24 02:07:29'),
(63, 24, 105, 102, 'unseen', '؟؟', '2018-07-24 02:07:36', '2018-07-24 02:07:36'),
(64, 23, 105, 106, 'unseen', 'هاي', '2018-07-24 02:08:04', '2018-07-24 02:08:04'),
(65, 23, 105, 106, 'unseen', 'تا', '2018-07-24 02:08:24', '2018-07-24 02:08:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_09_094510_create_countries_table', 1),
(4, '2018_06_10_091522_create_cities_table', 1),
(5, '2018_06_10_094520_create_admins_table', 1),
(6, '2018_06_10_094521_create_admin_password_resets_table', 1),
(7, '2018_06_10_094539_create_clients_table', 1),
(8, '2018_06_10_094540_create_client_password_resets_table', 1),
(9, '2018_06_10_110027_create_contacts_table', 1),
(10, '2018_06_10_110404_create_site_infos_table', 1),
(11, '2018_06_11_104656_create_friends_table', 1),
(12, '2018_06_11_104929_create_blacklists_table', 1),
(13, '2018_06_11_105114_create_success_stories_table', 1),
(14, '2018_06_12_105244_create_mails_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_views`
--

CREATE TABLE `profile_views` (
  `id` int(11) NOT NULL,
  `client_id` int(11) UNSIGNED NOT NULL,
  `profile_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_views`
--

INSERT INTO `profile_views` (`id`, `client_id`, `profile_id`, `created_at`, `updated_at`) VALUES
(12, 31, 0, '2018-06-27 20:38:10', '2018-06-27 20:38:10'),
(13, 34, 0, '2018-06-28 01:32:40', '2018-06-28 01:32:40'),
(14, 34, 2, '2018-06-28 01:54:09', '2018-06-28 01:54:09'),
(15, 34, 25, '2018-06-30 02:29:01', '2018-06-30 02:29:01'),
(18, 34, 34, '2018-06-30 15:08:15', '2018-06-30 15:08:15'),
(19, 34, 12, '2018-07-01 16:14:16', '2018-07-01 16:14:16'),
(20, 45, 0, '2018-07-02 17:34:20', '2018-07-02 17:34:20'),
(21, 45, 34, '2018-07-02 17:35:12', '2018-07-02 17:35:12'),
(22, 34, 45, '2018-07-02 17:40:26', '2018-07-02 17:40:26'),
(23, 49, 0, '2018-07-02 18:11:15', '2018-07-02 18:11:15'),
(24, 49, 47, '2018-07-02 19:08:05', '2018-07-02 19:08:05'),
(25, 49, 49, '2018-07-02 19:10:13', '2018-07-02 19:10:13'),
(28, 34, 9, '2018-07-03 23:03:10', '2018-07-03 23:03:10'),
(29, 34, 59, '2018-07-03 23:46:17', '2018-07-03 23:46:17'),
(73, 57, 0, '2018-07-17 10:50:22', '2018-07-17 10:50:22'),
(74, 92, 0, '2018-07-17 13:28:21', '2018-07-17 13:28:21'),
(75, 92, 59, '2018-07-17 14:20:17', '2018-07-17 14:20:17'),
(76, 92, 51, '2018-07-17 14:21:41', '2018-07-17 14:21:41'),
(77, 92, 31, '2018-07-17 14:21:49', '2018-07-17 14:21:49'),
(78, 92, 23, '2018-07-17 14:24:12', '2018-07-17 14:24:12'),
(79, 92, 58, '2018-07-17 14:29:12', '2018-07-17 14:29:12'),
(80, 92, 62, '2018-07-17 14:29:58', '2018-07-17 14:29:58'),
(81, 61, 0, '2018-07-17 15:10:36', '2018-07-17 15:10:36'),
(82, 61, 60, '2018-07-17 15:10:43', '2018-07-17 15:10:43'),
(83, 26, 0, '2018-07-17 15:13:29', '2018-07-17 15:13:29'),
(84, 27, 0, '2018-07-18 16:01:00', '2018-07-18 16:01:00'),
(85, 92, 103, '2018-07-23 21:53:44', '2018-07-23 21:53:44'),
(86, 105, 0, '2018-07-24 01:29:18', '2018-07-24 01:29:18'),
(87, 105, 51, '2018-07-24 01:31:40', '2018-07-24 01:31:40'),
(88, 105, 46, '2018-07-24 01:32:47', '2018-07-24 01:32:47'),
(89, 105, 103, '2018-07-24 01:37:30', '2018-07-24 01:37:30'),
(90, 106, 0, '2018-07-24 01:55:18', '2018-07-24 01:55:18'),
(91, 106, 105, '2018-07-24 01:55:56', '2018-07-24 01:55:56'),
(92, 105, 27, '2018-07-24 02:01:49', '2018-07-24 02:01:49'),
(93, 105, 104, '2018-07-24 02:02:06', '2018-07-24 02:02:06'),
(94, 105, 102, '2018-07-24 02:02:27', '2018-07-24 02:02:27'),
(95, 105, 106, '2018-07-24 02:08:50', '2018-07-24 02:08:50'),
(96, 107, 48, '2018-07-25 06:48:45', '2018-07-25 06:48:45'),
(97, 107, 26, '2018-07-25 06:49:33', '2018-07-25 06:49:33'),
(98, 107, 34, '2018-07-25 06:49:49', '2018-07-25 06:49:49'),
(99, 107, 31, '2018-07-25 06:50:18', '2018-07-25 06:50:18'),
(100, 107, 46, '2018-07-25 06:50:59', '2018-07-25 06:50:59'),
(101, 108, 0, '2018-07-25 17:59:09', '2018-07-25 17:59:09'),
(102, 108, 92, '2018-07-25 17:59:20', '2018-07-25 17:59:20'),
(103, 108, 46, '2018-07-25 17:59:28', '2018-07-25 17:59:28'),
(104, 108, 31, '2018-07-25 17:59:39', '2018-07-25 17:59:39'),
(105, 108, 34, '2018-07-25 17:59:48', '2018-07-25 17:59:48'),
(106, 108, 106, '2018-07-25 18:00:24', '2018-07-25 18:00:24'),
(107, 108, 48, '2018-07-25 18:01:43', '2018-07-25 18:01:43'),
(108, 108, 50, '2018-07-25 18:02:15', '2018-07-25 18:02:15'),
(109, 108, 49, '2018-07-25 18:02:46', '2018-07-25 18:02:46'),
(110, 108, 33, '2018-07-25 18:03:00', '2018-07-25 18:03:00'),
(111, 92, 108, '2018-07-25 20:58:48', '2018-07-25 20:58:48'),
(112, 92, 107, '2018-08-06 20:06:42', '2018-08-06 20:06:42');

-- --------------------------------------------------------

--
-- Table structure for table `site_infos`
--

CREATE TABLE `site_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fivicon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_words` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term_docx` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `policy` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_used` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_methods` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tw` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_infos`
--

INSERT INTO `site_infos` (`id`, `site_name`, `logo`, `fivicon`, `key_words`, `description`, `author`, `term_docx`, `policy`, `website_used`, `payment_methods`, `footer_about`, `fb`, `tw`, `instagram`, `google`, `created_at`, `updated_at`) VALUES
(1, 'هتجوز افضل مواقع الزواج', '1530016033.png', '1529419386.jpg', 'MARRY, TEST, DEMO', 'this is good site', 'RedaReda', '<p>أقسم بالله العظيم أني لم أدخل هذا الموقع الا بهدف الزواج الشرعي وفق كتاب الله و سنة رسوله , وليس لأي هدف آخر . وأعاهد الله وأعاهدكم على أن لا أضيع تعب الموقع , وأن لا أخدع الأعضاء , وأن أكون صادقا مع الله ثم مع نفسي , وأن ألتزم بشروط الموقع , و شروط المراسلة فيه , عسى ربي يكتب لي الخير في هذا المكان . والله خير الشاهدين</p>', 'good good greta', '<h1>أهلا بكم في موقع هتجوز...</h1>\r\n\r\n<p>هتجوز&nbsp;موقع عريق للتعارف والتواصل يجمع العالم على صفحاته لتوفير فرص زواج أكبر وأفضل بين العرب.هتجوز يقرب المسافات ويجمع القلوب. لديك فرص ذهبية للاختيار بين مليونين مشترك لتحديد من يتناسب مع شخصيتك ومتطلباتك. اشترك الآن لتصل لمبتغاك بغض النظر أين يكون.</p>', 'csacavcdav', 'موقع تعارف وتواصل عريق يضع الوطن العربي والعالم بين يديك من خلال الدردشة والمراسلة والمحادثة والشات للقاء شريك العمر والارتباط بالنصف الآخر و فارس الأحلام.', 'www.facebook.com', 'www.twitter.com', 'www.instagram.com', 'www.google.com', '2018-06-19 12:50:02', '2018-07-15 16:43:14');

-- --------------------------------------------------------

--
-- Table structure for table `success_stories`
--

CREATE TABLE `success_stories` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indexes for table `admin_to_client_messages`
--
ALTER TABLE `admin_to_client_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_to_client_receiver_id_foreign` (`receiver_id`),
  ADD KEY `admin_to_client_sender_id_foreign` (`sender_id`);

--
-- Indexes for table `blacklists`
--
ALTER TABLE `blacklists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blacklists_client_id_foreign` (`client_id`),
  ADD KEY `blacklists_friend_id_foreign` (`friend_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clients_email_unique` (`email`),
  ADD UNIQUE KEY `clients_phone_unique` (`phone`),
  ADD KEY `clients_city_id_foreign` (`city_id`);

--
-- Indexes for table `client_password_resets`
--
ALTER TABLE `client_password_resets`
  ADD KEY `client_password_resets_email_index` (`email`),
  ADD KEY `client_password_resets_token_index` (`token`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`),
  ADD KEY `friends_client_id_foreign` (`client_id`),
  ADD KEY `friends_friend_id_foreign` (`friend_id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mails_sender_id_foreign` (`sender_id`),
  ADD KEY `mails_receiver_id_foreign` (`receiver_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profile_views`
--
ALTER TABLE `profile_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_view_client_id_foreign` (`client_id`);

--
-- Indexes for table `site_infos`
--
ALTER TABLE `site_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `success_stories`
--
ALTER TABLE `success_stories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `success_stories_client_id_foreign` (`client_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_to_client_messages`
--
ALTER TABLE `admin_to_client_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `blacklists`
--
ALTER TABLE `blacklists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `profile_views`
--
ALTER TABLE `profile_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `site_infos`
--
ALTER TABLE `site_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `success_stories`
--
ALTER TABLE `success_stories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_to_client_messages`
--
ALTER TABLE `admin_to_client_messages`
  ADD CONSTRAINT `admin_to_client_receiver_id_foreign` FOREIGN KEY (`receiver_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `admin_to_client_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blacklists`
--
ALTER TABLE `blacklists`
  ADD CONSTRAINT `blacklists_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blacklists_friend_id_foreign` FOREIGN KEY (`friend_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `friends_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `friends_friend_id_foreign` FOREIGN KEY (`friend_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mails`
--
ALTER TABLE `mails`
  ADD CONSTRAINT `mails_receiver_id_foreign` FOREIGN KEY (`receiver_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mails_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile_views`
--
ALTER TABLE `profile_views`
  ADD CONSTRAINT `profile_view_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `success_stories`
--
ALTER TABLE `success_stories`
  ADD CONSTRAINT `success_stories_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
